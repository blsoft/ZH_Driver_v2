import configparser
import os
import platform

from driver.driver_utils import DriverUtils
from .serial_driver import SerialDriver


def get_config_file():
    if platform.system() == "Windows":
        appdata = os.environ.get("APPDATA")
        dir = os.path.join(appdata,"zh_2")
        if not os.path.exists(dir):
            os.mkdir(dir)
        return os.path.join(dir,"config.cfg")
    else:
        dir = "/etc/zh_2"
        if not os.path.exists(dir):
            os.mkdir(dir)
        return os.path.join(dir,"config.cfg")





def loadConfig():
    configFile = get_config_file()

    if platform.system() == "Windows":
        DriverUtils.check_exe()

    config = configparser.ConfigParser()
    if os.path.exists(configFile):
        print("Config file: " + os.path.abspath(configFile))
        config.read(configFile)
    else:
        print("Config not found, writing default config to file")
        config["General"] = {}
        general = config["General"]
        if platform.system() == "Windows":

            general["port"] = "COM4"
            general["baudrate"] = "9600"
            general['command'] = "av set {value}"
        else:
            general["port"] = "ttyUSB0"
            general["baudrate"] = "9600"
            general['command'] = "amixer -c 1 set Master {value}"
        with open(configFile, "w") as configfile:
            config.write(configfile)

    return config


def startDriver(cfg):
    drv = SerialDriver(cfg)


def driver_main():
    print("Starting ZH Multi Driver v1.0")
    print("Loadin config...")
    cfg = loadConfig()
    startDriver(cfg)



