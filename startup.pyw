import platform
import sys

import serial.tools.list_ports

import serial
from PyQt5.QtCore import Qt

from PyQt5.QtWidgets import QDialog, QApplication

from config_gui.cfg_gui_main import Ui_SettingsDialog


def setupUi(ui):
    ui.cbBaud.addItems(["2400", "4800", "9600", "19200", "38400", "57600", "115200", "230400", "460800", "921600"])
    ui.cbBaud.setCurrentIndex(2)

    #if platform.system() == "Windows":
    ports = serial.tools.list_ports.comports()
    ports.reverse()
    for n, (port, desc, hwid) in enumerate(ports, 1):
        ui.cbPort.addItem(port + " - " + hwid.split(":")[0])


def start_window():
    app = QApplication(sys.argv)

    dialog = QDialog()
    ui = Ui_SettingsDialog()



    ui.setupUi(dialog)

    setupUi(ui)

    dialog.setFixedSize(dialog.sizeHint())
    dialog.setWindowFlags(dialog.windowFlags() & (~ Qt.WindowContextHelpButtonHint))
    dialog.show()
    app.exec_()


if __name__ == "__main__":
    start_window()
